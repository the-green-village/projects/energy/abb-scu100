#Pymodbus 3 async code. #NIET TESTEN OP EEN UNIT DIE AL IN GEBRUIK IS
import asyncio
import logging
from logging.handlers import RotatingFileHandler
import pymodbus.client as ModbusClient
import pymodbus as pymodbus
import json
import time
import datetime as dt
import importlib
from typing import Dict, Tuple, List, Optional, Any

from tgvfunctions import tgvfunctions
from pymodbus.client import AsyncModbusTcpClient
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.exceptions import ModbusIOException
from pymodbus.pdu import ExceptionResponse
import pymodbus

# Relative imports
from projectsecrets import projectsecrets
import config
#from config import config
logging.basicConfig(
    format="%(asctime)s - %(funcName)s:%(lineno)s - %(levelname)s - %(message)s", level=logging.WARNING
)
logger = logging.getLogger(__file__)


class ConnectionException(Exception):
    pass

# Modbus TCP/IP server details
modbus_host = projectsecrets["MODBUS_HOST"]
modbus_port = projectsecrets["MODBUS_PORT"]

# Select all variables to read out commented ones won't be read out and produced
variable_selection = config.config["variable_selection"]
Asyncio_lock = asyncio.Lock()

# Name of all the mains variables
mains_selection = config.config["mains_selection"]

# Name Mapping

name_mapping = {"TRMS_Sensor": "TRMS Current",
    "AC_Sensor": "AC Current",
    "DC_Sensor": "DC Current",
    "TRMS_min_Sensor": 'TRMS Minimum Current',
    "AC_min_Sensor": 'AC Minimum Current',
    "DC_min_Sensor": 'DC Minimum Current',
    "TRMS_max_Sensor": 'TRMS Maximum Current',
    "AC_max_Sensor": 'AC Maximum Current',
    "DC_max_Sensor": 'DC Maximum Current',
    "TRMS_hold_Sensor": 'TRMS Hold Current',
    "AC_hold_Sensor": 'AC Hold Current',	
    "DC_hold_Sensor": 'DC Hold Current',
    "SID_version_bus_line_Sensor": 'SID Version Bus Line',
    "Polarity_(for_DC)_Sensor": 'Polarity (for DC)',
    "Active_Power_Sensor": 'Active Power Current',
    "Active_Energy_Sensor": 'Active Energy Current',
    "Active_Power_Group": 'Active Power Group',
    "Active_Energy_Group": 'Active Energy Group',
    "Branch_Name_Sensor": 'Branch Name',
    "Name_Group": 'Group Name',
    "Phase_assigned_Sensor": 'Phase Assigned',
    "Group_number_Sensor": 'Group Number',
    "Power_Factor_Sensor": 'Power Factor',
    "Mains": 'Mains',
    "PHASE VOLTAGE L1-N": 'Phase Voltage L1-N',
    "PHASE VOLTAGE L2-N": 'Phase Voltage L2-N',
    "PHASE VOLTAGE L3-N": 'Phase Voltage L3-N',
    "LINE CURRENT L1": 'Line Current L1',
    "LINE CURRENT L2": 'Line Current L2',
    "LINE CURRENT L3": 'Line Current L3',
    "LINE CURRENT L4/N": 'Line Current L4-N',
    "POWER FACTOR L1": 'Power Factor L1',
    "POWER FACTOR L2": 'Power Factor L2',
    "POWER FACTOR L3": 'Power Factor L3',
    "3-PHASE SUM APPARENT POWER": '3-Phase Sum Apparent Power',
    "APPARENT POWER L1": 'Apparent Power L1',
    "APPARENT POWER L2": 'Apparent Power L2',
    "APPARENT POWER L3": 'Apparent Power L3',
    "3-PHASE SUM ACTIVE POWER": '3-Phase Sum Active Power',
    "ACTIVE POWER L1": 'Active Power L1',
    "ACTIVE POWER L2": 'Active Power L2',
    "ACTIVE POWER L3": 'Active Power L3',
    "3-PHASE SUM REACTIVE POWER": '3-Phase Sum Reactive Power',
    "REACTIVE POWER L1": 'Reactive Power L1',
    "REACTIVE POWER L2": 'Reactive Power L2',
    "REACTIVE POWER L3": 'Reactive Power L3',
    "3-PHASE SUM ACTIVE ENERGY": '3-Phase Sum Active Energy',
    "3-PHASE SUM REACTIVE ENERGY": '3-Phase Sum Reactive Energy',
    "ACTIVE ENERGY L1": 'Active Energy L1',
    "ACTIVE ENERGY L2": 'Active Energy L2',
    "ACTIVE ENERGY L3": 'Active Energy L3',
    "REACTIVE ENERGY L1": 'Reactive Energy L1',
    "REACTIVE ENERGY L2": 'Reactive Energy L2',
    "REACTIVE ENERGY L3": 'Reactive Energy L3',
    "VOLTAGE THD L1 ": 'Voltage THD L1',
    "VOLTAGE THD L2": 'Voltage THD L2',
    "VOLTAGE THD L3": 'Voltage THD L3',
    "CURRENT THD L1 ": 'Current THD L1',
    "CURRENT THD L2": 'Current THD L2',
    "CURRENT THD L3": 'Current THD L3',
    "LINE CURRENT THD L4/N": 'Line Current THD L4-N',
    "CT ratio L1L2L3": 'CT Ratio L1-L2-L3',
    "CT ratio N": 'CT Ratio N',
    "3-PHASE SUM APPARENT ENERGY": '3-Phase Sum Apparent Energy',
    "APPARENT ENERGY L1": 'Apparent Energy L1',
    "APPARENT ENERGY L2": 'Apparent Energy L2',
    "APPARENT ENERGY L3": 'Apparent Energy L3',
    "ACTIVE ENERGY L1 100Wh": 'Active Energy L1 100Wh',
    "ACTIVE ENERGY L2 100Wh": 'Active Energy L2 100Wh',
    "ACTIVE ENERGY L3 100Wh": 'Active Energy L3 100Wh',
    "3-PHASE SUM ACTIVE ENERGY 100Wh": '3-Phase Sum Active Energy 100Wh',
    "REACTIVE ENERGY L1 100varh": 'Reactive Energy L1 100varh',
    "REACTIVE ENERGY L2 100varh": 'Reactive Energy L2 100varh',
    "REACTIVE ENERGY L3 100varh": 'Reactive Energy L3 100varh',
    "3-PHASE SUM REACTIVE ENERGY 100varh": '3-Phase Sum Reactive Energy 100varh',
    "APPARENT ENERGY L1 100VAh": 'Apparent Energy L1 100VAh',
    "APPARENT ENERGY L2 100VAh": 'Apparent Energy L2 100VAh',
    "APPARENT ENERGY L3 100VAh": 'Apparent Energy L3 100VAh',
    "3-PHASE SUM APPARENT ENERGY 100VAh ": '3-Phase Sum Apparent Energy 100VAh',}

#Standard functions from other projects
def timestamp(
    ) -> int:
    """
    Returns the current timestamp in milliseconds.

    Args:
        None

    Returns:
        int: current timestamp in milliseconds
    """
    return int(dt.datetime.now().timestamp() * 1e3)

# Deprecated for tgvfunctions.generate_measurement_dict
def build_measurement(
    measurement_id: str, 
    measurement_description: str, 
    unit: str, 
    value: float
    ) -> dict:
	"""
    Builds a dictionary representing a measurement.

    Args:
        measurement_id (str): The ID of the measurement.
        measurement_description (str): A description of the measurement.
        unit (str): The unit of the measurement.
        value (float): The value of the measurement.

    Returns:
        dict: A dictionary containing the measurement details.
    """
	return {
		"measurement_id": measurement_id,
		"measurement_description": measurement_description,
		"unit": unit,
		"value": value
	}

# Deprecated for asyncio.sleep
def sleep(
    start_time: float, 
    desired_time: float = 1
    ):
    """
    Pauses the execution to ensure a minimum time duration has passed since start_time.

    Args:
        start_time (float): The start time in seconds since the epoch (as returned by `time.time()`).
        desired_time (float, optional): The minimum time interval to wait, in seconds. Defaults to 1.0.

    If the elapsed time since start_time is less than desired_time, the function will sleep for the 
    remaining time to meet the desired duration. If the elapsed time exceeds desired_time, it logs a 
    warning indicating overshoot.
    """
    # Calculate elapsed time since start_time
    elapsed_time = time.time() - start_time
    
    # Determine remaining sleep time, if needed
    remaining_time = desired_time-elapsed_time
    
    if remaining_time>0: 
        # Sleep if there's still time left to reach the desired duration
        logging.debug(f"Sleeping for {remaining_time:.2f} seconds")
        time.sleep(remaining_time)
    else:
        # Log a warning if we have already exceeded the desired time        
        logging.warning(f"Overslept by {-remaining_time:.2f} seconds")


#-Modbus connection------------------------------------------------------        
def create_async_client(
    comm: str, 
    host: str, 
    port: int
    ):
    """
    Creates and configures an asynchronous Modbus client based on the specified communication type.

    Args:
        comm (str): Type of communication protocol ('tcp', 'udp', 'serial').
        host (str): Hostname or IP address (for 'tcp' and 'udp').
        port (int): Port number or serial port name (depending on communication type).

    Returns:
        ModbusClient: An instance of the appropriate asynchronous Modbus client.
                      Returns None if an invalid communication type is specified.

    Raises:
        ValueError: If an unsupported communication type is provided.
    """
    logging.debug(f"Creating async client for {comm} at {host}:{port}")
    
    client = None
    
    if comm == "tcp":
        client = AsyncModbusTcpClient(
            host,
            port=port,
            # timeout=10,
            # retries=3,
            # source_address=("localhost", 0),
        )
    elif comm == "udp":
        client = ModbusClient.AsyncModbusUdpClient(
            host,
            port=port,
            timeout=10,
            retries=3,
            # source_address=None,
        )
    elif comm == "serial":
        client = ModbusClient.AsyncModbusSerialClient(
            port,
            # timeout=10,
            # retries=3,
            baudrate=9600,
            bytesize=8,
            parity="N",
            stopbits=1,
            # handle_local_echo=False,
        )
    else:
        logging.error(f"Unknown communication type '{comm}' provided.")
        raise ValueError(f"Invalid communication type: {comm}")
    if client:  
        logging.debug(f"Successfully created async client for {comm} at {host}:{port}")
    return client

#-Helper functions------------------------------------------------------
def get_register_mapping_info(
    register_mapping: dict, 
    measurement: str
    ) -> tuple:
    """
    Retrieves register mapping information for a specified measurement.

    Args:
        register_mapping (dict): The register mapping data containing measurement details.
        measurement (str): The name of the measurement to retrieve information for.

    Returns:
        tuple: A tuple containing the following information about the measurement:
            - count (int): The number of registers for the measurement.
            - begin_register (int): The starting register address.
            - type_of_data (str): The type of data for the measurement.
            - end_register (int): The ending register address.
            - register_difference (int): The difference between the end and start register.

    Raises:
        KeyError: If the specified measurement is not found in the register mapping.
    """
    try:
        count = register_mapping[measurement]["count"]
        begin_register = register_mapping[measurement]["start_register"]
        type_of_data = register_mapping[measurement]["type"]
        end_register = register_mapping[measurement]["end_register"]
        register_difference = end_register - begin_register    
        return count, begin_register, type_of_data, end_register, register_difference     
    except KeyError as e:
        logging.error(f"Measurement '{measurement}' not found in register mapping: {e}")
        raise

def get_register_mapping(
    file_path: str
    ) -> dict:
    """
    Loads register mapping from a JSON file.

    Args:
        file_path (str): The path to the JSON file containing the register mapping.

    Returns:
        dict: A dictionary with the register mapping data.

    Raises:
        FileNotFoundError: If the specified file does not exist.
        json.JSONDecodeError: If the file contains invalid JSON.
    """
    try:
        with open(file_path, "r") as f:
            register_mapping = json.load(f)
        logging.debug(f"Register mapping loaded successfully from {file_path}")
    except FileNotFoundError:
        logging.error(f"File not found: {file_path}")
        raise
    except json.JSONDecodeError:
        logging.error(f"Invalid JSON in file: {file_path}")
        raise
    
    return register_mapping

def get_overflow_pairs(register_mapping: dict, measurement: str, register_keys: list) -> dict:
    overflow_pairs = {}
    for i, key in enumerate(register_keys): 
        mains_variable = register_mapping[measurement]["datapoints"][str(key)]["Description"]
        
        if '100' in mains_variable: #Deze een keer runnen en als dictionary meegeven.
            split_value = mains_variable.split(' 100')[0]

            for j, key in enumerate(register_keys):
                if split_value in register_mapping[measurement]["datapoints"][key]["Description"]:
                    split_index_in_keys = j
                    break

            
            overflow_pairs[split_value] = {
                "mains_variable_100": mains_variable,
                "mains_variable": split_value,
                "register_key_100": i,
                "register_key": split_index_in_keys
            }
    return overflow_pairs

async def reload_config():
    """Reloads the configuration from the config.py file in the same directory."""
    global config
    try:
        config = importlib.reload(config)
        logging.info("Configuration reloaded successfully.")
    except Exception as e:
        logging.error(f"Error reloading configuration: {e}")

async def get_sensor_names_for_documentation():
    """Gets the sensor names for documentation purposes in the format that you can easily copy and paste into a Markdown file as a table."""
    # Create Async modbus client
    logging.debug("Creating Async client")
    
    client = create_async_client("tcp", modbus_host, modbus_port)
    
    await client.connect()
    
    if not client.connected:
        raise ConnectionException("Could not establish modbus connection")
    
    logging.debug("Connected to Modbus server")

    # Fetch register mapping
    logging.debug("Getting register mapping")
    register_mapping = get_register_mapping(config.config["register_mapping_file"])

    # Fetch initial sensor mapping
    logging.info("Getting the sensor mapping")
    
    sensors_mapping, amount_of_named_sensors = await get_branch_name_mapping(client=client, register_mapping=register_mapping)
    
    print("|Sensor number| Name in ABB (same as name in electrical kabinet)| Dutch descriptive name| English descriptive name|")
    print("|---|----|----|----|")
    for i, key in enumerate(sensors_mapping):
        print(f"|{i+1}|{sensors_mapping[key]}|{sensors_mapping[key]}||")

async def get_time_till_update():
    # Fetch update time from config and parse it
    try:
        update_time = dt.datetime.strptime(config.config["update_time"], "%H:%M:%S").time()
    except ValueError:
        logging.warning("Invalid time format in configuration. Expected HH:MM:SS.")
        raise ValueError("Invalid time format in configuration. Expected HH:MM:SS.")
    
    update_datetime = dt.datetime.combine(dt.date.today(), update_time)
    current_time = dt.datetime.now()
    
    time_to_wait = (update_datetime - current_time).total_seconds()
    if time_to_wait < 0:
        time_to_wait += (24*3600)
    return time_to_wait
# Deprecated code, now use 'variable_selection_update' function
def variables_to_read_out(
    register_mapping: dict, 
    variable_selection: list
    ) -> list:
    """
    Filters and returns a list of selected variables to be read out from the register mapping.

    Args:
        register_mapping (dict): The dictionary containing available register variables.
        variable_selection (list): The list of variable names to select for reading.

    Returns:
        list: A list of variables that are both in the register mapping and selected for reading.
    """
    logging.debug("Fetching variables to read out")
    variables_read_out_interim = []
    
    variables_read_out_interim = [
        variable for variable in register_mapping if variable in variable_selection
    ]
    # Making sure the variables are not changed unitl the function is done.
    logging.debug(f"Variables selected for readout: {variables_read_out_interim}")
    return variables_read_out_interim

# Deprecated code, now use 'variable_selection_update' function
async def variables_to_read_out_async(
    register_mapping: Dict,  
    interval: int = 3600
    ):
    """
    Asynchronously reads and processes variables from a register mapping at a set interval.

    Args:
        register_mapping (dict): Dictionary containing register configuration details.
        variable_selection (list): List of variable names to be read and processed.
        interval (int): The interval (in seconds) to wait between reads. Defaults to 3600 (1 hour).
        
    Returns:
        None 
    """
    global variables_read_out

    while True:
        try:
            # Reloading the configuration to capture any changes in variable selection
            await reload_config()
            
            # Reload configuration to capture any changes in variable selection
            variable_selection = config.config["variable_selection"]
            updated_variables = variables_to_read_out(register_mapping, variable_selection)
            
            # Update the global variable in a thread-safe manner
            async with Asyncio_lock:
                variables_read_out = updated_variables
            
            logging.info(f"Updated global variables to read: {variables_read_out}")

            # Wait for the specified interval before the next check
            await asyncio.sleep(interval)
        except Exception as e:
            logging.error(f"Error updating variables to read: {e}")
            # Short delay before retrying in case of an error
            await asyncio.sleep(5)

#---Updating functions------------------------------------------------------
async def variable_selection_update(interval: int = 3600):
    """
    Asynchronously reads and processes variables from a register mapping at a set interval.

    Args:
        register_mapping (dict): Dictionary containing register configuration details.
        interval (int): The interval (in seconds) to wait between reads. Defaults to 3600 (1 hour).
        
    Returns:
        None 
    """
    global variable_selection

    while True:
        try:
            # Fetch time to wait till updating
            time_to_wait = await get_time_till_update()
            logging.info(f"Next time updating variables is scheduled for {time_to_wait/3600} hours from now.")
            # Reloading the configuration to capture any changes in mains selection
            await asyncio.sleep(time_to_wait)
            # Reloading the configuration to capture any changes in variable selection
            await reload_config()
            
            # Reload configuration to capture any changes in variable selection
            async with Asyncio_lock:
                variable_updated = config.config["variable_selection"]
            logging.info(f"Updated the variable selection as follows: {variable_updated}")	

            # await asyncio.sleep(interval)
        except Exception as e:
            logging.error(f"Error updating variable selection: {e}")
            await asyncio.sleep(5)
        
async def mains_selection_update(
    interval: int = 3600
    ):
    """
    Updated mains on every interval.
    
    Args:
        interval (int): The interval (in seconds) to wait between reads. Defaults to 3600 (1 hour).
        
    Returns:
        None 
    """
    global mains_selection
    
    while True:
        try:
            # Fetch update time from config and parse it
            time_to_wait = await get_time_till_update()
            logging.info(f"Next time updating mains is scheduled for {time_to_wait/3600} hours from now.")
            # Reloading the configuration to capture any changes in mains selection
            await asyncio.sleep(time_to_wait)
            await reload_config()
            
            
            mains_selection = config.config["mains_selection"]
            logging.info(f"Updated the mains selections as follows: {mains_selection}")	
        except Exception as e:
            logging.error(f"Error updating mains selection: {e}")
            await asyncio.sleep(5)
        
async def sensor_mapping_update(client, register_mapping, interval: int = 3600):
    """
    Updated the register mapping on every interval.
    
    Args:
        interval (int): The interval (in seconds) to wait between reads. Defaults to 3600 (1 hour).
        
    Returns:
        None 
    """
    global sensors_mapping
    global amount_of_named_sensors 
    while True:
        try:
            # Fetch time to wait till updating
            time_to_wait = await get_time_till_update()
            logging.info(f"Next time updating variables is scheduled for {time_to_wait/3600} hours from now.")
            await asyncio.sleep(time_to_wait)
            async with Asyncio_lock:
                sensors_mapping, amount_of_named_sensors = await get_branch_name_mapping(client=client, register_mapping=register_mapping)

            logging.info(f"Updated the sensor mapping as follows: {sensors_mapping}, with {amount_of_named_sensors} named sensors.")            
            # await asyncio.sleep(interval)
        except Exception as e:
            logging.error(f"Error updating register mapping: {e}")
            await asyncio.sleep(5)

#Deprecated code, now just checks sensor mapping hourly
def check_sensor_amounts(
    amount_of_sensors_from_mapping: int, 
    amount_of_sensors_from_data: int
    ) -> bool:
    """
    Checks if the number of sensors from the mapping matches the number of sensors from the data.

    Args:
        amount_of_sensors_from_mapping (int): Number of sensors expected from the mapping.
        amount_of_sensors_from_data (int): Number of sensors actually providing data.

    Returns:
        bool: True if the sensor counts match; False otherwise.
    """

    if amount_of_sensors_from_mapping == amount_of_sensors_from_data:
        logging.info("The number of named sensors matches the number of sensors providing data.")
        return True
    else:
        logging.error("The number of named sensors does not match the number of sensors providing data.")
        return False

#-Making the message------------------------------------------------------            
def make_message(
    data: dict, 
    measurement: str, 
    sensors_mapping: dict, 
    register_mapping: dict,
    producer,
    tgv
    ) -> None:
    """
    Creates a message dictionary from the given data based on the specified measurement type.

    Args:
        data (dict): The data dictionary containing sensor readings or other relevant information.
        measurement (str): The type of measurement (e.g., "Mains" or other types).
        sensors_mapping (dict): Mapping of sensors and their configurations.
        register_mapping (dict): Mapping of registers and their configurations.

    Returns:
        None
    """
    if measurement == "Mains":
        make_message_mains(
            data, 
            measurement, 
            sensors_mapping, 
            register_mapping=register_mapping,
            producer = producer,
            tgv = tgv
        )
    else:
        make_message_other(
            data, 
            measurement, 
            sensors_mapping, 
            register_mapping=register_mapping,
            producer = producer,
            tgv = tgv
    )     

def make_message_mains(
    data: dict, 
    measurement: str, 
    sensors_mapping: dict, 
    register_mapping: dict,
    producer,
    tgv
    ) -> None:
    """
    Creates a message dictionary containing mains electricity measurements and metadata.

    Args:
        data (dict): The data dictionary with measurement values for each sensor.
        measurement (str): The measurement type (e.g., "Mains").
        sensors_mapping (dict): Mapping of sensor configurations.
        register_mapping (dict): Mapping of registers and their configurations, including measurement descriptions and units.

    Returns:
        None

    Raises:
        KeyError: If `measurement` is not found in the `register_mapping`.
    """
    # Initalizing
    measurements = []
    metadata_value = {}
    # Retrieve data points for the specified measurement
    logging.debug(f"Retrieving the data points for the measurement: '{measurement}'")
    try:
        data_mains = register_mapping[measurement]["datapoints"]
    except KeyError:
        logging.error(f"Measurement '{measurement}' not found in register mapping.")
        raise
    logging.debug(f"The retrieved data points for the measurement: '{measurement}' is: {data_mains} ")

    # Construct measurements based on available data points
    logging.debug("Constructing measurements based on available data points.")
    for key, data_point in data_mains.items():
        description = data_point.get("Description")
        if description:
            #print("data_point", data_point )
            measurement_id = f"{name_mapping.get(description)}"
            measurement_description = f"Register: '{data_point.get('Addr (dec)')}', measuring: '{name_mapping.get(description)}', with original name: '{description}' (mains measurement register)"
            value = data.get(description)
            if value:
                unique_measurement = tgvfunctions.generate_measurement_dict(
                    measurement_id = measurement_id,
                    unit = data_point.get("Unit"),
                    value = data.get(description),
                    measurement_description = measurement_description
                )
                
                logging.debug(f"Creating the message for '{description}'")
                message = tgvfunctions.generate_kafka_message(
                    # Constant values for the device
                    project_id = config.config["project_id"],
                    application_id = config.config["application_id"],
                    device_id = config.config["device_id_mains"],
                    location_id = config.config["location_id"],
                    device_description =  config.config["device_description_mains"],
                    device_type = config.config["device_type"] + " (Mains)",
                    device_manufacturer = config.config["device_manufacturer"],
                    location_description = config.config["location_description"],
                    # Measurements changing with each message
                    timestamp=timestamp(), # Millisecond timestamp
                    measurements=[unique_measurement],
                )
                logging.debug(f"Produced the to Kafka for measurement '{measurement}' the following message: {message}")
                tgv.produce_fast(producer, message)
                
                measurements.append(unique_measurement)
        else: 
            logging.error(f"Skipping data point '{key}' as it lacks a description.")   
           
    metadata_value["measurements"] = measurements


def make_message_other(
    data: dict, 
    measurement: str, 
    sensors_mapping: dict, 
    register_mapping: dict,
    producer,
    tgv
    ) -> None:
    """
    Creates a message dictionary for non-mains measurements, including metadata and measurement data.

    Args:
        data (dict): Dictionary containing measurement values for each sensor.
        measurement (str): The measurement type (e.g., a sensor group or individual sensor).
        sensors_mapping (dict): Mapping of sensor names and identifiers.
        register_mapping (dict): Mapping containing measurement unit data.

    Returns:
        None
    Raises:
        KeyError: If `measurement` is not found in `register_mapping` or if `keys` are missing in `data`.
    """
    # Initalizing
    measurements = []
    metadata_value = {}
    
    data_measurement = register_mapping[measurement]["datapoints"]
    data_items = data_measurement.items()
    for key, data_point in data_items:
        sensor_number = data_point.get("Sensor number")
        sensor_name = sensors_mapping.get(sensor_number)
        sensor_value = data.get(sensor_number)
       
        if sensor_name:
            try:
                measurement_id = f"{name_mapping.get(measurement)}"
                measurement_description = (f"Register: '{data_point.get('Addr (dec)')}', measuring: '{name_mapping.get(measurement)}', at: '{sensor_name}', with original name: '{data_point.get('Description')}', and original sensor number: '{sensor_number}'")
                
                unique_measurement = tgvfunctions.generate_measurement_dict(
                    measurement_id=measurement_id,
                    unit=register_mapping[measurement]["unit"],
                    value=sensor_value,
                    measurement_description=measurement_description
                )
                measurements.append(unique_measurement)
        
                message = tgvfunctions.generate_kafka_message(
                    # Constant values for the device
                    project_id = config.config["project_id"],
                    device_id = config.config["device_id"] + f" - {sensor_name}",
                    application_id = config.config["application_id"],
                    location_id = config.config["location_id"],
                    device_description = f"Orginal sensor name: '{sensor_number}'",
                    device_type = config.config["device_type"],
                    device_manufacturer = config.config["device_manufacturer"],
                    location_description = config.config["location_description"],
                    # Measurements changing with each message
                    timestamp=timestamp(), # Millisecond timestamp
                    measurements=[unique_measurement],
                )    
                tgv.produce_fast(producer, message)
                logging.debug(f"Produced to KAFKA, for measurement: '{measurement}', the following message: {message}")
            except KeyError as e:
                logging.error(f"Missing data for '{key}' or unit information for '{measurement}': {e}")
                continue
    
    
#-Decoding the data------------------------------------------------------
def decode_registers(
    response, 
    num_registers: int = 96, 
    count:int = 1, 
    type_of_data: str = "other", 
    measurement: str = "other", 
    register_mapping: dict = None, 
    register_response = None
    ) -> list:
    """
    Decodes register values from a Modbus response based on the data type.

    Args:
        response: The raw Modbus response containing register data.
        num_registers (int): The number of registers to decode (default is 96).
        count (int): The count of registers to decode (default is 1).
        type_of_data (str): The data type for decoding, such as 'unsigned', 'signed', 'string', 'special', or 'short'.
        measurement (str): The specific measurement type to decode (default is "other").
        register_mapping (dict, optional): Dictionary containing register configurations (not used directly in decoding).
        register_response (optional): Response details, if additional context is needed for decoding.

    Returns:
        Decoded values based on the specified data type.

    Raises:
        ValueError: If `type_of_data` is unrecognized or unsupported.
    """
    if type_of_data == "unsigned":
        if measurement in {"Active_Power_Sensor", "Active_Energy_Sensor", "Active_Power_Group", "Active_Energy_Group"} or count == 2:
            decoded_values = decode_registers_unsigned_32(
                response, 
                num_registers, 
                count, 
                registers_response = register_response
            )
        else:
            decoded_values = decode_registers_unsigned(
                response, 
                num_registers, 
                count, 
                registers_response = register_response
            )
            
    elif type_of_data == "signed":
        decoded_values = decode_registers_signed(
            response, 
            num_registers, 
            count, 
            registers_response = register_response
        )
        
    elif type_of_data == "string":
        decoded_values = decode_string_registers(
            response, 
            num_registers, 
            count, 
            registers_response = register_response
        )
        
    elif type_of_data == "special":
        if measurement == "SID_version_bus_line_Sensor":
            decoded_values = decode_special_sid(response)
        elif measurement == "Polarity_(for_DC)_Sensor":
            decoded_values = decode_special_polarity(response)
        else:
            logging.warning(f"No special decoding method found for measurement '{measurement}'")
            return None
        
    elif type_of_data == "short":
        decoded_values = decode_registers_signed(
            response, 
            num_registers, 
            count
        )

    else:
        logging.warning(f"Type of data '{type_of_data}' is not recognized")
        raise ValueError(f"Unsupported data type: {type_of_data}")

    return decoded_values

# Decoding string data
def decode_string_registers(
    response, 
    num_registers: int, 
    count: int, 
    registers_response = None
    ) -> str:
    """
    Decodes string data from Modbus registers.

    Args:
        response: The raw Modbus response containing register data.
        num_registers (int): The total number of registers (not directly used but can provide context).
        count (int): The number of registers to decode into a string (each register holds 2 bytes).
        registers_response (optional): Alternative register data if provided (default is None).

    Returns:
        str: The decoded string, cleaned of any null characters or padding.

    Raises:
        ValueError: If the decoded string is empty or if an invalid response is given.
    """
    # Extract the registers
    if registers_response is None:
        registers = response.registers
    else:
        registers = registers_response

    # Ensure there are registers to decode
    if not registers:
        raise ValueError("No registers found in the response or provided registers.")
      
    # Create a decoder instance
    decoder = BinaryPayloadDecoder.fromRegisters(registers, byteorder=Endian.BIG)

    # Decode the data as a string (assuming ASCII or UTF-8 encoding)
    decoded_string = decoder.decode_string(count)  # Each register holds 2 bytes

    # Clean up any null characters or padding (optional)
    cleaned_string  = decoded_string.decode('ascii').rstrip('\x00').replace('\x00', '')
    
    return cleaned_string

# Decoding unsigned data
def decode_registers_unsigned(
    response, 
    num_registers: int, 
    count: int = 1, 
    registers_response = None
    ) -> list:
    """
    Decodes unsigned integer values from Modbus registers.

    Args:
        response: The raw Modbus response containing register data.
        num_registers (int): The total number of registers to decode.
        count (int): The number of registers to decode (default is 1).
        registers_response (optional): Alternative register data if provided (default is None).

    Returns:
        list: A list of decoded unsigned integer values.

    Raises:
        ValueError: If there are no registers to decode or if the number of registers is less than expected.
    """
    # Extract the registers
    if registers_response is None:
        registers = response.registers
    else:
        registers = registers_response
        
    # Ensure there are registers to decode
    if not registers:
        raise ValueError("No registers found in the response or provided registers.")
    
    # Create a decoder instance using Big Endian format
    decoder = BinaryPayloadDecoder.fromRegisters(registers, byteorder=Endian.BIG)
    
    # Decode the data as unsigned 16-bit integers
    decoded_values = []
    for _ in range(len(registers)): 
        value = decoder.decode_16bit_uint()  
        decoded_values.append(value)
        
    return decoded_values

def decode_registers_unsigned_32(
    response, 
    num_register: int,
    count: int = 1, 
    registers_response = None
    ) -> list:
    """
    Decodes unsigned 32-bit integer values from Modbus registers.

    Args:
        response: The raw Modbus response containing register data.
        num_registers (int): The total number of registers to decode (should be even for 32-bit values).
        count (int): The number of registers to decode (default is 1).
        registers_response (optional): Alternative register data if provided (default is None).

    Returns:
        list: A list of decoded unsigned 32-bit integer values.

    Raises:
        ValueError: If there are no registers to decode.
    """
    # Extract the registers
    if registers_response is None:
        registers = response.registers
    else: 
        registers = registers_response
        
    if not registers:
        raise ValueError("No registers found in the response or provided registers.")

    # Create a decoder instance using Big Endian format
    decoder = BinaryPayloadDecoder.fromRegisters(registers, byteorder=Endian.BIG, wordorder=Endian.LITTLE)
    
    # Decode the data as unsigned 32-bit integers
    decoded_values = []
    for _ in range(len(registers) // 2):
        value = decoder.decode_32bit_uint()
        # Adjusting for specific condition, changing the max int from 4294934526 to 32767 as later code checks for the value 32767/
        if value == 4294934527 or value == 65536:
            value = 32767
            
        decoded_values.append(value)

    return decoded_values

# Decoding signed data
def decode_registers_signed(
    response, 
    num_registers: int, 
    count: int = 1, 
    registers_response = None
    ) -> list:
    """
    Decodes signed integer values from Modbus registers.

    Args:
        response: The raw Modbus response containing register data.
        num_registers (int): The total number of registers to decode.
        count (int): The number of registers to decode (default is 1).
        registers_response (optional): Alternative register data if provided (default is None).

    Returns:
        list: A list of decoded signed integer values.

    Raises:
        ValueError: If there are no registers to decode.
    """
    # Extract the registers
    if registers_response is None:
        registers = response.registers
    else:
        registers = registers_response
        
    # Ensure there are registers to decode
    if not registers:
        raise ValueError("No registers found in the response or provided registers.")
            
    # Create a decoder instance using Big Endian format
    decoder = BinaryPayloadDecoder.fromRegisters(registers, byteorder=Endian.BIG)

    # Decode the data as signed 16-bit integers
    decoded_values = []
    for _ in range(len(registers)):
        value = decoder.decode_16bit_uint()
        if value == 65536:
            value = 32767
        decoded_values.append(value)

    return decoded_values

# Special data 
# Type: 'Polarity_(for_DC)_Sensor'
def decode_special_polarity(
    response
    ) -> list:
    """
    Decodes the polarity from Modbus registers.

    Args:
        response: The raw Modbus response containing register data.

    Returns:
        list: A list of decoded polarity states as strings ("Direct", "Reverse", or "Unknown DC polarity state").

    Raises:
        ValueError: If no registers are found in the response.
    """
    registers = response.registers

    # Ensure there are registers to decode
    if not registers:
        raise ValueError("No registers found in the response.")

    decoded_values = []
    for register in registers:
        if register == 32767:
            decoded_values.append(32767)
            continue
        
        # Extract the polarity bit (bit 0)
        polarity = register & 0x8000  # Extract the MSB which holds the polarity
        
        if polarity == 0x0000:
            # "Direct: DC current coming out of the cone is positive"
            decoded_values.append("Direct")
        elif polarity == 0x8000:
            # "Reverse: DC current coming out of the cone is negative"
            decoded_values.append("Reverse")
        else:
            logging.warning("Unknown DC polarity state")
            decoded_values.append("Unknown DC polarity state")
    return decoded_values    

# Type: 'SID_version_bus_line_Sensor'
def decode_special_sid(
    response
    ) -> dict:
    """
    Decodes special SID (Sensor Identification) information from Modbus registers.

    Args:
        response: The raw Modbus response containing register data.

    Returns:
        dict: A dictionary containing sensor information, including:
              - SID
              - Hardware Version
              - Software Version
              - Measurement Range
              - Enabled Data Types
              - Bus Line ID

    Raises:
        ValueError: If no registers are found in the response.
    """
    # Extract the registers
    registers = response.registers

    # Ensure there are registers to decode
    if not registers:
        raise ValueError("No registers found in the response.")
    
    sensor_info = {}
    # Decode Serial Number (offset 0h, 4 words)
    sensor_info['SID'] = decode_sid(registers[0:4])

    # Decode Hardware Version (offset 4h, 2 words)
    sensor_info['HW_Version'] = decode_hw_version(registers[4:6])

    # Decode Software Version (offset 6h, 3 words)
    sensor_info['SW_Version'] = decode_sw_version(registers[6:9])

    # Decode Measurement Range (offset 9h, 1 word)
    sensor_info['Measurement_Range'] = decode_measurement_range(registers[9:10])

    # Decode Enabled Data Types (offset Ah, 1 word)
    sensor_info['Enabled_Data_Types'] = decode_enabled_data_types(registers[10:11])

    # Reserved words (offset Bh, 4 words) are skipped, no need to decode
    
    # Decode Bus Line ID (offset Fh, 1 word)
    sensor_info['Bus_Line_ID'] = decode_bus_line_id(registers[15:16])

    return sensor_info

def decode_sid(
    registers: list
    ) -> str:
    """
    Decodes a unique serial number from a list of Modbus registers.

    Args:
        registers (list): A list of registers containing the serial number.
                          It is expected to have at least 4 registers.

    Returns:
        str: A hexadecimal string representing the unique serial number.
    """
    # Decode the 4-word unique serial number
    sid = ''.join(f'{register:04X}' for register in registers[:4])
    return sid

def decode_hw_version(
    registers: list
    ) -> str:
    """
    Decodes the hardware version from a list of Modbus registers.

    Args:
        registers (list): A list of registers containing the hardware version.
                          It is expected to have at least 2 registers.

    Returns:
        str: A string representing the hardware version in the format 'major.minor'.

    """
    # Decode the 2-word hardware version.
    major = registers[0]
    minor = registers[1]
    return f'{major}.{minor}'

def decode_sw_version(
    registers: list
    ) -> str:
    """
    Decodes the software version from a list of Modbus registers.

    Args:
        registers (list): A list of registers containing the software version.
                          It is expected to have at least 3 registers.

    Returns:
        str: A string representing the software version in the format 'major.minor.patch'.
    """
    # Decode the 3-word software version.
    major = registers[0]
    minor = registers[1]
    patch = registers[2]
    return f'{major}.{minor}.{patch}'

def decode_measurement_range(
    registers: list
    ) -> float:
    """
    Decodes the measurement range from a list of Modbus registers.

    Args:
        registers (list): A list of registers containing the measurement range.
                          It is expected to have at least 1 register.

    Returns:
        float: The measurement range in amperes (A), calculated as the value in 
               the register multiplied by 0.1.

    """
    # Decode the 1-word measurement range (in 0.1A steps).
    return registers[0] * 0.1  # Assuming it's in 0.1A steps

def decode_enabled_data_types(
    registers: list
    ) -> int:
    """
    Decodes the enabled data types from a list of Modbus registers.

    Args:
        registers (list): A list of registers containing the enabled data types.
                          It is expected to have at least 1 register.

    Returns:
        int: The value representing the enabled data types.
    """
    # Decode the 1-word enabled data types. 
    return registers[0]  # Specific interpretation depends on the InSite Bus Protocol

def decode_bus_line_id(
    registers: list
    ) -> str:
    """
    Decodes the bus line ID from a list of Modbus registers.

    Args:
        registers (list): A list of registers containing the bus line ID.
                          It is expected to have at least 1 register.

    Returns:
        str: A string representing the bus line ID. Possible values are:
             - "No sensor connected" for bus line ID 0
             - "Line 1" for bus line ID 1
             - "Line 2" for bus line ID 2
             - "Unknown bus line ID: X" for any other value X.

    """
    
    # Decode the 1-word bus line ID.
    bus_line_id = registers[0]
    if bus_line_id == 0:
        return "No sensor connected"
    elif bus_line_id == 1:
        return "Line 1"
    elif bus_line_id == 2:
        return "Line 2"
    else:
        return f"Unknown bus line ID: {bus_line_id}"
    

#-Getting the data-----------------------------------------------------
async def get_data(
    client, 
    measurement: str, 
    register_mapping: dict, 
    sensors_mapping: dict, 
    begin_register: Optional[int] = None, 
    count: int = 96) -> Any:
    """
    Asynchronously retrieves and decodes raw data from a Modbus client based on a specified measurement.
    
    Args:
        client: The Modbus client instance for reading holding registers.
        measurement (str): The specific measurement to retrieve (e.g., sensor type).
        register_mapping (dict): Dictionary containing register configuration details.
        sensors_mapping (dict): A mapping of sensor names or IDs to measurement data.
        begin_register (int, optional): The starting register address. If None, it is set from `register_mapping`.
        count (int): Number of registers to read. Defaults to 96.

    Returns:
        Any: The decoded data for the specified measurement.
    """
    # Retrieve mapping information based on the measurement parameter
    count, begin_register, type_of_data, end_register, register_difference  = get_register_mapping_info(register_mapping, measurement)
    
    # Initialize results
    start_time = time.time()
    
    # Determine the correct function to retrieve and decode raw data based on measurement type
    if measurement == "SID_version_bus_line_Sensor":
        raw_data = await get_data_raw_special_sid(
            client, 
            measurement, 
            register_mapping
        ) # Decoding in the same function
    elif measurement == "Mains":
        raw_data = await get_data_raw_mains(
            client, 
            measurement, 
            register_mapping
        )
    elif measurement in {"Branch_Name_Sensor", "Name_Group"}:
        raw_data = await get_data_raw_string(
            client, 
            measurement, 
            register_mapping
        ) # Decoding in the same function
    elif measurement in {"Active_Power_Sensor", "Active_Energy_Sensor", "Active_Power_Group", "Active_Energy_Group"}:
        raw_data = await get_data_raw_count_2(
            client, 
            measurement, 
            register_mapping
        ) # Decoding in the same function
    else:
        raw_data = await get_data_raw(
            client, 
            measurement, 
            register_mapping, 
            begin_register= begin_register
        )
        
    logging.debug(f"Time to retrieve raw data: {time.time() - start_time:.4f} seconds")
    
    # If already decoded, skip decoding; otherwise, decode the raw data
    start_time_decoding = time.time()
    if measurement in {"Branch_Name_Sensor", "Name_Group", "SID_version_bus_line_Sensor", "Active_Power_Sensor", "Active_Power_Group", "Active_Energy_Sensor", "Active_Energy_Group", "Mains"}:
        decoded_data = raw_data # Data is already decoded in retrieval function
    else:
        decoded_data = decode_registers(
            raw_data, 
            register_difference, 
            count, 
            type_of_data=type_of_data, 
            measurement=measurement
        )
    logging.debug(f"Time to decode data: {time.time() - start_time_decoding:.4f} seconds")
    return decoded_data
    
async def get_data_raw(
    client, 
    measurement: str, 
    register_mapping: dict, 
    begin_register: Optional[int] = None, 
    count: int = 96
    )-> Optional[Any]:
    """
    Asynchronously reads raw data from Modbus holding registers for a specified measurement.

    Args:
        client: The Modbus client instance for reading holding registers.
        measurement (str): The measurement key to identify within register_mapping.
        register_mapping (dict): Dictionary containing register configuration details.
        begin_register (int, optional): The starting register address. If None, it will be set from `register_mapping`.
        count (int): Number of registers to read. Defaults to 96.

    Returns:
        Optional[Any]: The response from reading the holding registers, or None if an exception occurs.
    """
    # If begin_register is not provided, get it and other configuration details from the register mapping
    if begin_register is None:
        count, begin_register, type_of_data, end_register, register_difference  = get_register_mapping_info(register_mapping, measurement)
        count = register_difference # Set the count to the register difference, as is possible to read the entire range in one go
    try:
        # Measure and log time taken to read registers
        start_time = time.time()
        response = await client.read_holding_registers(
            begin_register, 
            count=count, 
            slave=1
        )
        logging.debug(f"Time to read registers at {begin_register}: {time.time() - start_time}")
    except ModbusIOException as e:
        logging.warning(f"ModbusIOException encountered at register {begin_register}: {e}")
        return None
    
    return response

async def get_data_raw_string(
    client, 
    measurement: str, 
    register_mapping: dict
    )-> List:
    """
    Asynchronously reads and decodes string data from Modbus registers for a specified measurement.
    
    Args:
        client: The Modbus client instance for reading holding registers.
        measurement (str): The measurement key within the register mapping.
        register_mapping (dict): Dictionary containing register configuration details.

    Returns:
        List[str]: A list of decoded strings from the specified registers.
    """
    # Retrieve mapping information based on the measurement parameter
    count, begin_register, type_of_data, end_register, register_difference  = get_register_mapping_info(register_mapping, measurement)
    
    # Initialize results
    decoded_data = list()
    
    try:
        # Loop through registers in specified intervals
        for register_address in range(begin_register, end_register, count):
            # Read a block of registers asynchronously
            response = await client.read_holding_registers(register_address, count=count, slave=1)
            
            # Check for errors in the Modbus response
            if response.isError():
                logging.warning(f"Received Modbus library error: {response}")
                return []
            
            # Handle Modbus exception messages specifically            
            if isinstance(response, pymodbus.ExceptionResponse):
                logging.warning(f"Received Modbus library exception: {response}")  # THIS IS NOT A PYTHON EXCEPTION, but a valid modbus message
                continue # Continue to the next register range if an exception is received
            
            # Decode the register response data into a string
            decoded_string = decode_registers(
                response = response, 
                count = count, 
                type_of_data = type_of_data, 
                measurement = measurement
            )
            decoded_data.append(decoded_string)
            
    except ModbusIOException as e:
        logging.warning(f"ModbusIOException encountered at register {register_address}: {e}")
    return decoded_data

async def get_branch_name_mapping(
    client, 
    register_mapping: dict, 
    measurement: str="Branch_Name_Sensor"
    ) -> Tuple[Dict[str, str], int]:
    """
    Retrieves a mapping of branch names (sensor names) by reading registers from a client
    asynchronously and decoding the data.

    Args:
        client: The client object for reading holding registers.
        register_mapping (dict): A dictionary containing the register mappings and measurement info.
        measurement (str): The measurement key to look up in register_mapping. Defaults to "Branch_Name_Sensor".

    Returns:
        Tuple[Dict[str, str], int]: 
            - A dictionary mapping sensor numbers to decoded branch names.
            - The count of successfully named sensors.
    """
    # Retrieve mapping information based on the measurement parameter
    count, begin_register, type_of_data, end_register, register_difference  = get_register_mapping_info(register_mapping, measurement)

    # Initialize results and counters
    sensor_name_mapping = dict()
    named_sensor_count = 0
    register_keys = list(register_mapping[measurement]["datapoints"].keys())
    decoded_strings= []
    total_responses = []

    # Collect register data in chunks, reading registers asynchronously
    for i in range(begin_register, end_register, count):
        start_time = time.time()
        total_response = await asyncio.gather(
            client.read_holding_registers(i + 0 * count, count = count, slave = 1),
        )
        logging.debug(f"Time to read a registers: {time.time() - start_time}, {i}")

        for response in total_response:
            total_responses.append(response.registers)
    
    # Decode strings from each register response
    for register_response in total_responses:
        decoded_string = decode_string_registers(
            response = None,
            num_registers= register_difference, 
            count = count, 
            registers_response=register_response
            )
        decoded_strings.append(decoded_string)
   
    # Map decoded strings to sensor numbers
    for i in range(len(decoded_strings)):
        decoded_string = decoded_strings[i]
        if len(decoded_string) > 0:
            if "Current sensor" not in decoded_string: # 'Current sensor' is the standard name assigned during setup, does not mean it has a channel.
                sensor_number = register_mapping[measurement]["datapoints"][str(register_keys[i])]["Sensor number"]
                sensor_name_mapping[sensor_number] = decoded_string
                named_sensor_count +=1
                logging.debug(f"Decoded sensor {sensor_number}: {decoded_string}")

       
    logging.debug(f"The amount of named sensors is: {named_sensor_count}")
    return sensor_name_mapping, named_sensor_count

async def get_data_raw_special_sid(
    client, 
    measurement: str, 
    register_mapping: dict
    )-> List:
    """
    Asynchronously retrieves and decodes raw data from a client for a specific measurement
    by reading Modbus holding registers.

    Args:
        client: The Modbus client instance used to read registers.
        measurement (str): The measurement key in the register mapping.
        register_mapping (dict): Dictionary containing register configurations for measurements.

    Returns:
        List: A list of decoded data points for the specified measurement.
    """
    # Retrieve mapping information based on the measurement parameter
    count, begin_register, type_of_data, end_register, register_difference  = get_register_mapping_info(register_mapping, measurement)
    
    # Initialize results and counters
    decoded_data = list()
    
    # Loop through the specified register range    
    for register_address in range(begin_register, end_register, count):
        try:
            # Read the registers asynchronously
            response = await client.read_holding_registers(register_address, count=count, slave=1)
            
            # Decode the response data
            decoded_sid_special = decode_registers(
                response = response, 
                count = count, 
                type_of_data = type_of_data, 
                measurement = measurement
            )
            
            # Append the decoded data to the results list
            decoded_data.append(decoded_sid_special)
        except ModbusIOException as e:
            logging.warning(f"ModbusIOException encountered at register {register_address}: {e}")
    return decoded_data

async def get_data_raw_count_2(
    client, 
    measurement: str, 
    register_mapping: dict
    )->List:
    """
    Asynchronously reads and decodes data from a client for a specific measurement
    by retrieving data from two register blocks.

    Args:
        client: The Modbus client instance for reading holding registers.
        measurement (str): The measurement key in the register mapping.
        register_mapping (dict): Dictionary containing register configuration details for measurements.

    Returns:
        List: A list of decoded data points from the specified register blocks.
    """
    # Retrieve mapping information based on the measurement parameter    
    count, begin_register, type_of_data, end_register, register_difference = get_register_mapping_info(register_mapping, measurement)
    
    # Initialize results and counters    
    decoded_data = list()
    calculated_register_size = int(register_difference/count)
    total_decoded = list()
    try:
        # Gather register data from two adjacent blocks
        start_time_new = time.time()
        response_1 = await get_data_raw(client, measurement, register_mapping, begin_register=begin_register, count=96)
        response_2 = await get_data_raw(client, measurement, register_mapping, begin_register=begin_register + 96, count=96)
        
        decoded_data_1 = decode_registers(
            response_1, 
            register_difference, 
            count, 
            type_of_data=type_of_data, 
            measurement=measurement
        )
        decoded_data_2 = decode_registers(
            response_2, 
            register_difference, 
            count, 
            type_of_data=type_of_data, 
            measurement=measurement
        )
        total_decoded_data = decoded_data_1 + decoded_data_2
        end_time_new = time.time()
        # start_time_old = time.time()
        # responses = await asyncio.gather(
        #     client.read_holding_registers(begin_register, count=calculated_register_size, slave=1),
        #     client.read_holding_registers(begin_register +96, count=calculated_register_size, slave=1)
        # )
        # # Decode and aggregate the data from both register blocks
        # for response in responses:
        #     decoded_block = decode_registers(
        #         response = response, 
        #         count = calculated_register_size, 
        #         type_of_data = type_of_data, 
        #         measurement = measurement
        #     )
        #     decoded_data += decoded_block
        # if decoded_data == total_decoded_data:
        #     print("True")
        # else:
        #     print("False")
        # end_time_old = time.time()
        # print(f"Time to read and decode new method: {end_time_new - start_time_new:.4f} seconds, time to read and decode old method: {end_time_old - start_time_old:.4f} seconds")
    except ModbusIOException as e:
          logging.warning(f"ModbusIOException encountered during data retrieval: {e}, for either register: {begin_register} or {begin_register + calculated_register_size}")
    
    return total_decoded_data

async def get_data_raw_mains(
    client, 
    measurement: str, 
    register_mapping: dict
    )-> List[float]:
    """
    Asynchronously retrieves and decodes raw mains data from Modbus registers.
    
    Args:
        client: The Modbus client instance for reading holding registers.
        measurement (str): The specific measurement key to identify within register_mapping.
        register_mapping (dict): Dictionary containing register configuration details.

    Returns:
        List[float]: A list of decoded and scaled values for the mains data.
    """
    # Retrieve mapping information based on the measurement parameter    
    count, begin_register, type_of_data, end_register, register_difference  = get_register_mapping_info(register_mapping, measurement)
    
    # Initialize results and counters        
    decoded_data = []
    register_keys = list(register_mapping[measurement]["datapoints"].keys())
    
    try:

        total_response = await asyncio.gather(
            client.read_holding_registers(36866, count=113, slave=1),
            client.read_holding_registers(36980, count=82, slave=1))
        # Flatten the list of register values. for now also checking code for correctness

        total_response_registers_new = [register for response in total_response for register in response.registers]
        
        # Apply hardcoded slicing logic to extract the relevant registers
        total_response_registers = (
            total_response_registers_new[0:5] + 
            total_response_registers_new[13:27] + 
            total_response_registers_new[35:63] + 
            total_response_registers_new[112:124] + 
            total_response_registers_new[126:143] + 
            total_response_registers_new[141:143] + 
            total_response_registers_new[163:195]
        )      
        
        # Getting the data pairs:
        overflow_pairs= get_overflow_pairs(register_mapping, measurement, register_keys)
        
        # Decode the register response data
        for i, register_key in enumerate(register_keys):
            
            data_point = register_mapping[measurement]["datapoints"][register_key]
            type_of_data = data_point["Format"]
            count = data_point["Words (16bit)"]
            mains_variable = data_point["Description"]
            resolution = data_point["Resolution"]
            
            if mains_variable not in mains_selection:
                logging.debug(f"Skipping variable {mains_variable}")               
                decoded_data.append(None)
                continue
            
            logging.debug(f"Reading variable {mains_variable}")
            
            # Decode primary register value
            start_index = i*count
            sliced_response = total_response_registers[start_index:start_index+count]
            decoded_values = decode_registers(
                response = None, 
                count = count, 
                type_of_data = type_of_data, 
                measurement = measurement, 
                register_response = sliced_response
            )
            scaled_value = decoded_values[0] * resolution # List with only one item
            
            if mains_variable in overflow_pairs:
                overflow_info = overflow_pairs[mains_variable]
                overflow_index = overflow_info["register_key_100"]
                overflow_datapoint = register_mapping[measurement]["datapoints"][str(register_keys[overflow_index])]
                
                # Decode overflow register value
                overflow_resolution = overflow_datapoint["Resolution"]
                overflow_variable = overflow_datapoint["Description"]
                overflow_count = overflow_datapoint["Words (16bit)"]
                overflow_type_of_data = overflow_datapoint["Format"]
                overflow_start = overflow_index * overflow_count
               
                
                overflow_response = total_response_registers[overflow_start:overflow_start+overflow_count]
                decoded_overflow = decode_registers(
                    response = None, 
                    count = overflow_count, 
                    type_of_data = overflow_type_of_data, 
                    measurement = measurement, 
                    register_response = overflow_response
                )
                remainder  = round(scaled_value % overflow_resolution, 4) # The scaled decoded value with everything above the tens removed, e.g. 324.56, becomes 24.56
                roundness_check = remainder // (0.5 * overflow_resolution) # Checks if the tens_scaled_decoded_values is above or below the middle of the overflow_resolution, if above returns 1 if below returns 0
                scaled_value = remainder  + (decoded_overflow[0] - roundness_check)*overflow_resolution           
            
            decoded_data.append(scaled_value)
    except (ModbusIOException, IndexError, KeyError) as e:
        logging.warning(f"Error encountered: {e}")

    return decoded_data

#-Getting the amount of sensors providing data-------------------------------------
def get_amount_of_sensors_providing_data_from_mapped_data(
    mapped_data: dict
    ) -> int:
    """
    Counts the number of sensors providing data from the mapped data.

    Args:
        mapped_data (dict): A dictionary where keys represent sensor identifiers 
                            and values contain the corresponding data. 

    Returns:
        int: The count of sensors that have provided data (non-None values).
    """
    # Use a generator expression to count non-None values efficiently
    return sum(1 for value in mapped_data.values() if value is not None)


#-Mapping the data-----------------------------------------------------
def map_data(
    decoded_data: list, 
    measurement: str, 
    register_mapping: dict, 
    sensors_mapping: dict
    ) -> dict:
    """
    Map decoded data to a structured dictionary based on measurement type and register mapping.

    Args:
        decoded_data (list): The decoded data from the sensors.
        measurement (str): The type of measurement being processed.
        register_mapping (dict): A mapping of registers to sensor data information.
        sensors_mapping (dict): A mapping of sensors to their respective identifiers.

    Returns:
        dict: A dictionary mapping sensor numbers to their respective decoded values.
    """

    # Retrieve mapping information based on the measurement parameter
    count, begin_register, type_of_data, end_register, register_difference  = get_register_mapping_info(register_mapping, measurement)
    resolution = register_mapping[measurement]["resolution"]
    data = {}
    
    if measurement == "Mains":
        # Mapping for Mains measurement
        main_keys_list = list(register_mapping[measurement]["datapoints"].keys())
        for i in range(len(main_keys_list)):
            description = register_mapping[measurement]["datapoints"][main_keys_list[i]]["Description"]
            data[description] = decoded_data[i]
    else:
        # General mapping for other measurements
        for i in range(begin_register, end_register, count):
            k = (i - begin_register) // count  # Calculate the index for decoded_data
            sensor_number = register_mapping[measurement]["datapoints"][str(i)]["Sensor number"]
            decoded_value = decoded_data[k]

            # Handling different types of data
            if type_of_data in ["unsigned", "signed", "short"]:
                # Non-special types require scaling and null checks
                if decoded_value != (32767 if type_of_data in ["signed"] else 0):
                    data[sensor_number] = decoded_value * resolution
                else:
                    data[sensor_number] = None
                    
            elif type_of_data == "string":
                # Strings have no scaling
                data[sensor_number] = decoded_value if decoded_value else None
                
            elif type_of_data == "special":
                # Special cases that require individual checks
                if measurement == "SID_version_bus_line_Sensor":
                    data[sensor_number] = decoded_value if decoded_value["Enabled_Data_Types"] != 32767 else None
                elif measurement == "Polarity_(for_DC)_Sensor":
                    data[sensor_number] = decoded_value if decoded_value != 32767 else None
                    
            else:
                logging.warning(f"Type of data {type_of_data} is not recognized")

    return data

#-Makeing Measurements-----------------------------------------------------
async def make_measurements(
    client, 
    measurement: str, 
    register_mapping: dict, 
    sensors_mapping: dict, 
    historic_amount_of_sensors_providing_data: int, 
    task_results: list,
    producer,
    tgv
    ) -> None:
    """
    Asynchronously gather and process sensor measurements.

    Args:
        client: The client used to fetch data.
        measurement (str): The type of measurement being processed.
        register_mapping (dict): A mapping of registers to sensor data information.
        sensors_mapping (dict): A mapping of sensors to their respective identifiers.
        historic_amount_of_sensors_providing_data (int): Historical count of sensors providing data.
        task_results (list): List to append the results.

    Returns:
        None
    """
    # Retrieve mapping information based on the measurement parameter
    count, begin_register, type_of_data, end_register, register_difference  = get_register_mapping_info(register_mapping, measurement)
    
    """ Not needed anymore as now updates on time instead of amount of sensors providing data
    # Initialize the amount of sensors providing data    
    amount_of_sensors_providing_data_from_mapped_data = historic_amount_of_sensors_providing_data
    """
    # Initalizing stats
    # Get the raw data
    logging.debug(f"Getting the raw data for {measurement}")
    start_time_decode = time.time()
    
    decoded_data = await get_data(client, measurement, register_mapping, sensors_mapping, begin_register, count)
    
    time_decoding = time.time() - start_time_decode
    logging.debug(f"Time spent getting data for '{measurement}' is: {time_decoding:.2f} seconds")
    
    # Map the data
    time_start_mapping = time.time()
    
    mapped_data = map_data(decoded_data, measurement, register_mapping, sensors_mapping)
    
    time_mapping = time.time() - time_start_mapping
    logging.debug(f"Time spent mapping data for '{measurement}' is: {time_mapping:.2f} seconds")
    
    return mapped_data, measurement, sensors_mapping, register_mapping
    
#-Send to Kafka-----------------------------------------------------------
async def send_to_kafka(
    client, 
    measurement: str, 
    register_mapping: dict, 
    sensors_mapping: dict, 
    historic_amount_of_sensors_providing_data: int, 
    task_results: list,
    producer,
    tgv,
    desired_time: float,
    start_time: float
    ):
    """
    Gathers measurement data, sends it to Kafka, and manages timing.

    Args:
        client: The Modbus client.
        measurement: The type of measurement to retrieve.
        register_mapping: The mapping of registers.
        sensors_mapping: The mapping of sensors.
        historic_amount_of_sensors_providing_data: The historic count of sensors providing data.
        task_results: The list to store task results.
        producer: The Kafka producer instance.
        tgv: The TGV instance for producing messages.
        desired_time: The desired interval between sends.
        start_time: The start time of the current cycle.
    """
    iteration = 0
    total_time = 0
    while True:
        try:
            start_time = time.time()
            
            if measurement in variable_selection:
                logging.debug(f"Making measurements for {measurement}")

                mapped_data, measurement, sensors_mapping, register_mapping = await make_measurements(
                    client = client, 
                    measurement = measurement, 
                    register_mapping= register_mapping, 
                    sensors_mapping = sensors_mapping, 
                    historic_amount_of_sensors_providing_data = historic_amount_of_sensors_providing_data, 
                    task_results = task_results,
                    producer = producer, 
                    tgv = tgv
                    )
                make_message(mapped_data, measurement, sensors_mapping, register_mapping, producer, tgv)

            interval_time = time.time() - start_time
            total_time += interval_time
            iteration +=1

            # Manage timing
            elapsed_time = (time.time() - start_time)
            sleep_time = max(0, desired_time - elapsed_time)
            if -elapsed_time + desired_time < 0:
                logging.warning(f"For '{measurement}' the elapsed time is: {elapsed_time:.2f} seconds, desired run frequency is: {1/desired_time:.2f} Hz, frequency this run is: {1/(time.time() - start_time):.2f} Hz")
            if measurement in variable_selection:
                logging.info(f"Time to make measurements for '{measurement}' is: {interval_time:.2f} seconds, average time is: {total_time/iteration:.2f} seconds, would support {1/(time.time() - start_time):.2f} Hz, and on average: {1/(total_time/iteration):.2f} Hz.")
                logging.info(f"The time spent sleeping is: {sleep_time:.2f} seconds, desired run frequency is: {desired_time:.2f} seconds, time spent this run is: {time.time() - start_time:.2f} seconds")
                await asyncio.sleep(sleep_time)
            else:
                await asyncio.sleep(sleep_time)
            
        except Exception as e:
            logging.error(f"Error during sending to Kafka: {e}")    
        finally:
            if measurement in variable_selection:
                logging.info(f"For '{measurement}', total time is: {time.time() - start_time:.2f} seconds, average time is: {total_time/iteration:.2f} seconds, and on average: {1/(total_time/iteration):.2f} Hz.")
    
#-Main(s)-----------------------------------------------------
async def main_async():
    """
    Main asynchronous function to connect to Modbus server, get sensor mappings,
    and perform measurements in a loop.
    """
    start_time_total = time.time()

    # Initalizing Kafka
    logging.debug("Initalizing KAFKA")
    topic = projectsecrets["TOPIC"]
    tgv = tgvfunctions.TGVFunctions(topic)

    producer_name = config.config["producer_name"]
    producer = tgv.make_producer(producer_name)

    # Create Async modbus client
    logging.debug("Creating Async client")
    
    client = create_async_client("tcp", modbus_host, modbus_port)
    
    await client.connect()
    
    if not client.connected:
        raise ConnectionException("Could not establish modbus connection")
    
    logging.debug("Connected to Modbus server")
    
    # Fetch remaining config setup
    desired_frequency = config.config["desired_frequency"]
    desired_time = 1/desired_frequency
    
    # Fetch register mapping
    logging.debug("Getting register mapping")
    register_mapping = get_register_mapping(config.config["register_mapping_file"])

    # Fetch initial sensor mapping
    logging.debug("Getting the sensor mapping")
    start_sensor_mapping = time.time()
    sensors_mapping, amount_of_named_sensors = await get_branch_name_mapping(client=client, register_mapping=register_mapping)
    sensor_mapping_time = time.time() - start_sensor_mapping

    logging.info(f"Sensor mapping is done, there are {amount_of_named_sensors} named sensors, the mapping is as follows: {sensors_mapping}, it took {sensor_mapping_time:.2f} seconds.")
 
    # Track execution metrics
    total_run_time = 0
    iteration = 0
    try:
        # Main loop
        start_time = time.time()
        task_results = []
        # Execute tasks concurrently
        """ This code only works after python 3.11, left in incase the server ever updates
        async with asyncio.TaskGroup() as tg:
            for variable in register_mapping.keys():
                tg.create_task(
                    send_to_kafka(
                        client, variable, register_mapping, sensors_mapping,
                        amount_of_named_sensors, task_results, producer, tgv, 
                        desired_time, start_time
                    )
                )
            if config.config["update"]:
                # Run mapping refresh every hour (via configurable interval)
                tg.create_task(variable_selection_update())
                tg.create_task(mains_selection_update())
                tg.create_task(sensor_mapping_update(client= client, register_mapping=register_mapping))
        """  
        # Following code is done so that it works on a lower version of python   
        tasks = []

        # Create tasks for sending to Kafka
        for variable in register_mapping.keys():
            tasks.append(
                send_to_kafka(
                    client, variable, register_mapping, sensors_mapping,
                    amount_of_named_sensors, task_results, producer, tgv, 
                    desired_time, start_time
                )
            )

        # Add mapping refresh tasks if update is enabled
        if config.config["update"]:
            tasks.extend([
                variable_selection_update(),
                mains_selection_update(),
                sensor_mapping_update(client=client, register_mapping=register_mapping)
            ])

        # Gather all tasks
        await asyncio.gather(*tasks)

        # Log task results if available
        for task in task_results:
            logging.debug(f"Task results: {task[0]}, amount of sensors providing data: {task[1]}")
        
        iteration += 1
        run_time = time.time() - start_time
        total_run_time += run_time
        logging.debug(f"Loop {iteration} took {run_time:.2f} seconds, average: {total_run_time / iteration:.2f}")
            
    except pymodbus.ModbusException as exc:
        logging.error(f"Received ModbusException({exc}) from library")
    
    except KeyboardInterrupt:
        logging.error("Aborted by user")
        
    except Exception as e:
        logging.error(f"Unexpected error: {e}")
    finally:
        logging.debug("Cleaning up resources")
        producer.flush()
        client.close()
        logging.info(f"Total runtime: {time.time() - start_time_total:.2f} seconds")


if __name__ == "__main__":
    asyncio.run(
        main_async(), debug=True
    )