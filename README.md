# ABB SCU100

## Table of Contents
- [Notes](#notes)
- [Modbus Producer](#modbus-producer)
    - [Set-up](#set-up)
    - [Data](data)
        - [Example message](#example-message)
    - [Libraries](#libraries)
- [Excel Parser](#excel-parser)
- [Register Mapping (JSON Parser)](#register-mapping-json-parser)
- [Existing setups](#existing-set-ups)
    - [Veldkast D](#veldkast-d)
    - [Veldkast I](#veldkast-i)
    - [Co Creation Centre](#co-creation-centre-ccc)
    - [Garage](#garage)
- [Legacy](#legacy)
    - [Legacy read-me](#legacy-read-me)

## Notes
This producer was "finished" on 25/11/2024 and written for the following product: "ABB SCU100", firmware version 1.8.0.

**Known problems**

Around 10% of the time getting the data from the ABB SCU100 is way slower, which causes the average time to be slower by a factor 15. This was tested through the use of the program: 'abb-test.py' it can be found in the test folder.

Some ABBs are not updated to the latest version as of 25/11/2024

To find legacy code take a look at: https://gitlab.com/the-green-village/projects/energy/abb-scu100-testing 

## Modbus producer
### Set-up
For the set-up  of a new ABB SCU-100, you need to configure 2 files: `config.py` and `projectsecrets.py`, you can get `projectsecrets.py` from `projectsecrets.py.template` and `config.py` from `config.py.template`. In general very few settings need to be changed and when set-up the producer will automatically update and produce when new sensors are added. It also automatically checks if the settings are changed every interval, currently 3600 seconds.

For the set up make a virtual environment using the following command: `python -m venv env`, and then install the required libraries using: `pip install -r requirements.txt`, finally it can be run using: `modbus3_producer.sh`. This is the most up to date version of the modbus consumer, it includes asynchronous programming and a refractor for readability and to update 'Pymodbus' to version 3.x.x.

When setting up change the file directory in `modbus3_producer.sh` also change the names of the filepaths.

#### config.py.template

It was chosen to use a template file as a lot of these producers will be set-up in the Green Village.
In general the following variables need to be changed when setting up a new instance of this producer:

`application_id = "ABB SCU100 - #LOCATION NAME",
location_id= "#LOCATION NAME",
location_description = "#LOCATION OF ABB UNIT",
register_mapping_file = r"#FILEPATH/register_mapping_ABB-SCU-100.json",
`

#### projectsecrets.py.template
These are kept in a projectsecrets file to prevent them from being leaked.

`projectsecrets = dict(
MODBUS_HOST = '#FILL IN IP ADRESSS', # IP Adress
MODBUS_PORT = '#FILL IN PORT NUMBER', # Port
TOPIC = '#FILL IN TOPIC TO PRODUCE TO' # Kafka Topic
)`


### Data

The available data consists of two parts, the variables to read out and a selection of mains to read out.

**Variable selection**
|**Variable**|**Sent to KAFKA**|**Config name**|**Comment**|
|--|------------|---------|--|
|TRMS Current| Yes|TRMS_Sensor||
|AC Current|Yes|AC_Sensor||
|DC Current|Yes|DC_Sensor||
|TRMS Minimum Current|No|TRMS_min_Sensor||
|AC Minimum Current|No|AC_min_Sensor||
|DC Minimum Current|No|DC_min_Sensor||
|Trms Maximum Current|No|TRMS_max_Sensor||
|AC Maximum Current|No|AC_max_Sensor||
|DC Maximum Current|No|DC_max_Sensor||
|TRMS Hold Curent|No|TRMS_hold_Sensor|Reads but gives weird data reads data from sensor 65 to 81 instead of 1 to 17|
|AC Hold Current|No|AC_hold_Sensor|Reads but gives weird data from other sensors then 1 to 17|
|DC Hold Current|No|DC_hold_Sensor|Reads and gives data from sensor 1 to 17, but instead of returning int max (32767) from sensor >=18 it returns 0|
|SID Version Bus Line |No|SID_version_bus_line_Sensor|Works, but not for a high Hz|
|Polarity (for DC)|No|Polarity_(for_DC)_Sensor||
|Active Power Current|Yes|Active_Power_Sensor||
|Active Energy Current|Yes|Active_Energy_Sensor||
|Active Power Group|No|Active_Power_Group|Mapping amount of sensors does not work as no groups|
|Active Energy Group|No|Active_Energy_Group|Mapping amount of sensors does not work as no groups|
|Branch Name|No|Branch_Name_Sensor||
|Group Name|No|Name_Group|Mapping amount of sensors does not work as no groups|
|Phase Assigned|No|Phase_assigned_Sensor||
|Group Number|No|Group_number_Sensor||
|Power Factor|No|Power_Factor_Sensor||
|Mains|Yes|Mains||




|**Main**|**Send to KAFKA**|**Config name**|**Comments**|
|-|-|-|-|
|Phase Voltage L1-N|Yes|PHASE VOLTAGE L1-N||
|Phase Voltage L2-N|Yes|PHASE VOLTAGE L2-N||
|Phase Voltage L3-N|Yes|PHASE VOLTAGE L3-N||
|Line Current L1|Yes|LINE CURRENT L1||
|Line Current L2|Yes|LINE CURRENT L2||
|Line Current L3|Yes|LINE CURRENT L3||
|Line Current L4-N|Yes|LINE CURRENT L4/N||
|Power Factor L1|Yes|POWER FACTOR L1||
|Power Factor L2|Yes|POWER FACTOR L2||
|Power Factor L3|Yes|POWER FACTOR L3||
|3-Phase Sum Apparent Power|Yes|3-PHASE SUM APPARENT POWER||
|Apparent Power L1|Yes|APPARENT POWER L1||
|Apparent Power L2|Yes|APPARENT POWER L2||
|Apparent Power L3|Yes|APPARENT POWER L3||
|3-Phase Sum Active Power|Yes|3-PHASE SUM ACTIVE POWER||
|Active Power L1|Yes|ACTIVE POWER L1||
|Active Power L2|Yes|ACTIVE POWER L2||
|Active Power L3|Yes|ACTIVE POWER L3||
|3-Phase Sum Reactive Power|Yes|3-PHASE SUM REACTIVE POWER||
|Reactive Power L1|Yes|REACTIVE POWER L1||
|Reactive Power L2|Yes|REACTIVE POWER L2||
|Reactive Power L3|Yes|REACTIVE POWER L3||
|3-Phase Sum Active Energy|Yes|3-PHASE SUM ACTIVE ENERGY||
|3-Phase Sum Reactive Energy|Yes|3-PHASE SUM REACTIVE ENERGY||
|Active Energy L1|Yes|ACTIVE ENERGY L1||
|Active Energy L2|Yes|ACTIVE ENERGY L2||
|Active Energy L3|Yes|ACTIVE ENERGY L3||
|Reactive Energy L1|Yes|REACTIVE ENERGY L1||
|Reactive Energy L2|Yes|REACTIVE ENERGY L2||
|Reactive Energy L3|Yes|REACTIVE ENERGY L3||
|Voltage THD L1|Yes|VOLTAGE THD L1||
|Voltage THD L2|Yes|VOLTAGE THD L2||
|Voltage THD L3|Yes|VOLTAGE THD L3||
|Current THD L1|Yes|CURRENT THD L1||
|Current THD L2|Yes|CURRENT THD L2||
|Current THD L3|Yes|CURRENT THD L3||
|Line Current THD L4-N|Yes|LINE CURRENT THD L4/N||
|CT Ratio L1-L2-L3|Yes|CT ratio L1L2L3||
|CT Ratio N|Yes|CT ratio N||
|3-|Yes|3-PHASE SUM APPARENT ENERGY||
|Apparent Energy L1|Yes|APPARENT ENERGY L1||
|Apparent Energy L2|Yes|APPARENT ENERGY L2||
|Apparent Energy L3|Yes|APPARENT ENERGY L3||
|Active Energy L1 100Wh|No|ACTIVE ENERGY L1 100Wh|Coupled to: 'ACTIVE ENERGY L1'|
|Active Energy L2 100Wh|No|ACTIVE ENERGY L2 100Wh|Coupled to: 'ACTIVE ENERGY L2'|
|Active Energy L3 100Wh|No|ACTIVE ENERGY L3 100Wh|Coupled to: 'ACTIVE ENERGY L3'|
|3-Phase Sum Active Energy 100Wh|No|3-PHASE SUM ACTIVE ENERGY 100Wh|Coupled to: '3-PHASE SUM ACTIVE ENERGY'|
|Reactive Energy L1 100varh|No|REACTIVE ENERGY L1 100varh|Coupled to: 'REACTIVE ENERGY L1'|
|Reactive Energy L2 100varh|No|REACTIVE ENERGY L2 100varh|Coupled to: 'REACTIVE ENERGY L2'|
|Reactive Energy L3 100varh|No|REACTIVE ENERGY L3 100varh|Coupled to: 'REACTIVE ENERGY L3'|
|3-Phase Sum Reactive Energy 100varh|No|3-PHASE SUM REACTIVE ENERGY 100varh|Coupled to: '3-PHASE SUM REACTIVE ENERGY'|
|Apparent Energy L1 100VAh|No|APPARENT ENERGY L1 100VAh|Coupled to: 'APPARENT ENERGY L1'|
|Apparent Energy L2 100VAh|No|APPARENT ENERGY L2 100VAh|Coupled to: 'APPARENT ENERGY L2'|
|Apparent Energy L3 100VAh|No|APPARENT ENERGY L3 100VAh|Coupled to: 'APPARENT ENERGY L3'|
|3-Phase Sum Apparent Energy 100Vah|No|3-PHASE SUM APPARENT ENERGY 100VAh|Coupled to: '3-PHASE SUM APPARENT ENERGY '|


#### Example message
Note: For scalability on the kafka side every measurement has its own message to KAFKA.

`{'project_id': 'energy', 'application_id': 'ABB SCU100 - Veldkast I', 'device_id': 'SCU100 (Mains)', 'timestamp': 1732550859485, 'project_description': None, 'application_description': None, 'device_description': 'Mains Electricity Measurements', 'device_manufacturer': 'ABB', 'device_type': 'SCU100 (Mains)', 'device_serial': None, 'location_id': 'Veldkast I', 'location_description': 'Veldkast I', 'latitude': None, 'longitude': None, 'altitude': None, 'measurements': [{'measurement_id': 'Apparent Energy L1', 'measurement_description': "Register: '37032', measuring: 'Apparent Energy L1', with original name: 'APPARENT ENERGY L1' (mains measurement register)", 'unit': 'VAh', 'value': 229.53}]}`
### Libraries
The libraries and the version that are used in this project are as follows:
|Library|Version|Comments|
|------------|----------|-----|
|pymodbus|3.6.9||
|confluent-kafka|2.5.3||
|openpyxl|3.1.3||
|tgvfunctions|v0.1.2||

# Excel Parser
**Can be found at https://gitlab.com/the-green-village/projects/energy/abb-scu100-testing**
In support of this project an excel parser was written with a couple of functions that may be reused in other places.

It includes functions for doing the following:
- Getting column names
- Getting column names and exporting as JSON
- Reading only specified columns based on a given JSON file
- Indexing the data in a sheet, by row number and keys given by headers
- Replacing the indexing

You can find the code under: `~\parse_excel\parse_excel.py`, all the functions output to a subdirectory: `~\json_outputs`.

# Register Mapping (JSON Parser)
**Can be found at https://gitlab.com/the-green-village/projects/energy/abb-scu100-testing**
In support of this project a JSON Parser was written to get the register mapping into the correct form.

It includes functions for doing the following:
- Remove part of the description
- Adding sensor numbers to the description
- Removing unwanted nested key, value pairs
- Changed part of the description
- Refractoring of JSON objects
- Splitting JSON files into chunks
- Returning values from nested keys

You can find the code under: `~\register_mapping\register_mapping.py`, all the functions output to a subdirectory: `~\register_mapping_output`. The legacy code can be found at: `~\register_mapping\make_register_mapping.py`.

# Existing set-ups
|Location| Firmware version|Comments|
|---|---|---|
|Veldkast D|1.3.2|Currently running old producer.|
|Veldkast I|1.8.0|Has a warning that a current sensor has not been named|
|Co Creation Centre (CCC)|1.8.0||
|Garage|1.8.0||

## Veldkast D
|Sensor number| Name in ABB (same as name in electrical kabinet)| Dutch descriptive name| English descriptive name|
|---|----|----|----|
|1|Huisnummer 28|Huisnummer 28|Housenumber 28 - L1|
|2|Huisnummer 28|Huisnummer 28|Housenumber 28 - L2|
|3|Huisnummer 28|Huisnummer 28|Housenumber 28 - L3|
|4|Huisnummer 30|Huisnummer 30|Housenumber 30 - L1|
|5|Huisnummer 30|Huisnummer 30|Housenumber 30 - L2|
|6|Huisnummer 30|Huisnummer 30|Housenumber 30 - L3|
|7|Huisnummer 32|Huisnummer 32|Housenumber 32 - L1|
|8|Huisnummer 32|Huisnummer 32|Housenumber 32 - L2|
|9|Huisnummer 32|Huisnummer 32|Housenumber 32 - L3|
|10|DC Container DC installatie|DC Container DC installatie - L1|DC Container DC installation - L1|
|11|DC Container DC installatie|DC Container DC installatie - L2|DC Container DC installation - L2|
|12|DC Container DC installatie|DC Container DC installatie - L3|DC Container DC installation - L3|
|13|Openbare verlichting|Openbare verlichting|Public Lighting|
|14|Openbare verlichting|Openbare verlichting|Public Lighting|
|15|Openbare verlichting|Openbare verlichting|Public Lighting|
|16|Openbare verlichting|Openbare verlichting|Public Lighting|
|17|DC Container verlichting & cd's|DC Container verlichting & cd's| DC Containt Lighting & cd's|
## Veldkast I
|Sensor number| Name in ABB (same as name in electrical kabinet)| Dutch descriptive name| English descriptive name|  
|---|----|----|----|
|1|Wandcontactdozen 230V|Wandcontactdozen 230V|Electrical sockets 230V|
|2|ICT|ICT|ICT|
|3|Reserve|Reserve|Reserve|
|4|Reserve|Reserve|Reserve|
|5|Reserve|Reserve|Reserve|
|6|Tiler Laadtegel|Tiler Laadtegel|Tiler Loadingtile|
|7|PowerParking Carport Kast L1|PowerParking Carport Kast L1|PowerParking Carport Case L1|
|8|PowerParking Carport Kast L2|PowerParking Carport Kast L2|PowerParking Carport Case L1|
|9|PowerParking Carport Kast L3|PowerParking Carport Kast L3|PowerParking Carport Case L1|
|10|WS Laadpaal (Gridfeeder) L1|Laadpaal (Gridfeeder) L1|Charging Station (Gridfeeder) L1|
|11|WS Laadpaal (Gridfeeder) L2|Laadpaal (Gridfeeder) L2|Charging Station (Gridfeeder) L2|
|12|WS Laadpaal (Gridfeeder) L3|Laadpaal (Gridfeeder) L3|Charging Station (Gridfeeder) L3|
|13|Reserve L1|Reserve L1|Reserve L1|
|14|Reserve L2|Reserve L2|Reserve L2|
|15|Reserve L3|Reserve L3|Reserve L3|
|16|Matelab L1|MATElab L1|MATElab L1|
|17|Matelab L2|MATElab L2|MATElab L2|
|18|Matelab L3|MATElab L3|MATElab L3|
|19|CEE-form 16A L1|CEE-form 16A L1|CEE-form 16A L1|
|20|CEE-form 16A L2|CEE-form 16A L2|CEE-form 16A L2|
|21|CEE-form 16A L3|CEE-form 16A L3|CEE-form 16A L3|
|22|CEE-form 32A L1|CEE-form 32A L1|CEE-form 32A L1|
|23|CEE-form 32A L2|CEE-form 32A L2|CEE-form 32A L2|
|24|CEE-form 32A L3|CEE-form 32A L3|CEE-form 32A L3|
|25|Reserve L1|Reserve L1|Reserve L1|
|26|Reserve L2|Reserve L2|Reserve L2|
|27|Reserve L3|Reserve L3|Reserve L3|




## Co Creation Centre (CCC)
|Sensor number| Name in ABB (same as name in electrical kabinet)| Dutch descriptive name| English descriptive name|  
|---|----|----|----|
|1|Klimaattoren 63A - L1|Klimaattoren 63A - L1|Climate tower 63A - L1|
|2|Klimaattoren 63A - L2|Klimaattoren 63A - L2|Climate tower 63A - L2|
|3|Klimaattoren 63A - L3|Klimaattoren 63A - L3|Climate tower 63A - L3|
|4|Klimaattoren 25A - L1|Klimaattoren 25A - L1|Climate tower 25A - L1|
|5|Klimaattoren 25A - L2|Klimaattoren 25A - L2|Climate tower 25A - L2|
|6|Klimaattoren 25A - L3|Klimaattoren 25A - L3|Climate tower 25A - L3|
|7|Klimaattoren 25A - L1|Klimaattoren 25A - L1|Climate tower 25A - L1|
|8|Klimaattoren 25A - L2|Klimaattoren 25A - L2|Climate tower 25A - L2|
|9|Klimaattoren 25A - L3|Klimaattoren 25A - L3|Climate tower 25A - L3|
|10|Lichtlijn Co-Creation Centre - L|Lichtlijn Co-Creation Centre - L1|Lightline Co-Creation Centre - L1|
|11|Lichtlijn Co-Creation Centre - L|Lichtlijn Co-Creation Centre - L2|Lightline Co-Creation Centre - L2|
|12|RESERVE - L3|Reserve - L3|Reserve - L3|
|13|WCD's vloerpotten Co-Creation Ce|WCD's vloerpotten Co-Creation Centre - L1|Subfloor outler Co-Creation Centre - L1|
|14|WCD's vloerpotten Co-Creation Ce|WCD's vloerpotten Co-Creation Centre - L2|Subfloor outler Co-Creation Centre - L2|
|15|WCD's vloerpotten Co-Creation Ce|WCD's vloerpotten Co-Creation Centre - L3|Subfloor outler Co-Creation Centre - L3|
|16|WCD's vloerpotten Co-Creation Ce|WCD's vloerpotten Co-Creation Centre - L1|Subfloor outler Co-Creation Centre - L1|
|17|WCD locatie dak - L2|WCD locatie dak - L2|Electrical socket on roof - L2|
|18|RESERVE - L3|Reserve - L3|Reserve - L3|
|19|Patchkast - L1|Patchkast - L1|Patchbox - L1|
|20|CEE-form Co-Creation Centre - L1|CEE-form Co-Creation Centre - L1|CEE-form Co-Creation Centre - L1|
|21|CEE-form Co-Creation Centre - L2|CEE-form Co-Creation Centre - L2|CEE-form Co-Creation Centre - L2|
|22|CEE-form Co-Creation Centre - L3|CEE-form Co-Creation Centre - L3|CEE-form Co-Creation Centre - L3|
|23|Regelkast - L1|Regelkast - L1|Controlbox - L1|
|24|Regelkast - L2|Regelkast - L2|Controlbox - L2|
|25|Regelkast - L3|Regelkast - L3|Controlbox - L3|
|26|Zonwering - L1|Zonwering - L1|Sun protection - L1|
|27|Zonwering - L2|Zonwering - L2|Sun protection - L2|
|28|WCD op kabelgoot - L3|WCD op kabelgoot - L3|Electrical Socket in cable gutter - L3|
|29|CEE-form op kabelgoot - L1|CEE-form op kabelgoot - L1|CEE-form in cable gutter - L1|
|30|Verlichting + wcd's servicepavil|Verlichting + wcd's servicepaviljoen - L2|Lighting + Electrical Socket service pavilion - L2|
|31|Waterbehandeling boiler servicep|Waterbehandeling boiler servicepaviljoen - L3|Water treatment boiler service pavilion - L3|
|32|Kanaalverwarming servicepaviljoe|Kanaalverwarming servicepaviljoen - L1|Channel heating service pavilion - L1|
|33|Elektrische radiator servicepavi|Elektrische radiator servicepaviljoen - L2|Electrical heater service pavilion - L2|
|34|WTW unit servicepaviljoen - L3|WTW unit servicepaviljoen - L3|WTW unit service pavilion - L3|
|35|Warmtepomp - L1|Warmtepomp - L1|Heatpump - L1|
|36|Warmtepomp - L2|Warmtepomp - L2|Heatpump - L2|
|37|Warmtepomp - L3|Warmtepomp - L3|Heatpump - L3|
|38|Field Factors - L2|Field Factors - L2|Field Factors - L2|

## Garage
|Sensor number| Name in ABB (same as name in electrical kabinet)| Dutch descriptive name| English descriptive name|
|---|----|----|----|
|1|Technische ruimte - L1|Technische ruimte - L1|Technical room - L1|
|2|Boiler - L2|Boiler - L2|Boiler - L2|
|3|Elektrische verwarming toilet - |Elektrische verwarming toilet - |Electrical heating toiler - |
|4|SER / ICT - L1|SER / ICT - L1|SER / ICT - L1|
|5|Verlichting kantoor, toilet, bui|Verlichting kantoor, toilet, buiten|Lighting office, toilet, outside|
|6|Verlichting 1 & 2 - L3|Verlichting 1 & 2 - L3|Lichting 1 & 2 - L3|
|7|Verlichting 3, 4 & 5 - L1|Verlichting 3, 4 & 5 - L1|Lichting 3, 4 & 5 - :1|
|8|Afzuiging box 3 & kantoor - L2|Afzuiging box 3 & kantoor - L2|Extraction box 3 & office - L2|
|9|RESERVE - L3|Reserve - L3|Reserve - L3|
|10|RESERVE - L1|Reserve - L1|Reserve - L1|
|11|Infraroodpanelen - L2|Infraroodpanelen - L2|Infraredpanel - L2|
|12|WCD's wandgoot kantoor - L1|WCD's wandgoot kantoor - L1|Electricalsocket wall gutter office - L1|
|13|WCD's kantoor - L2|WCD's kantoor - L2|Electrical socket - L2|
|14|WCD's box 1 - L3|WCD's box 1 - L3|Electrical socket box 1 - L3|
|15|WCD's box 2 - L1|WCD's box 2 - L1|Electrical socket box 2 - L1|
|16|WCD's box 3 - L2|WCD's box 3 - L2|Electrical socket box 3 - L2|
|17|WCD's box 4 - L3|WCD's box 4 - L3|Electrical socket box 4 - L3|
|18|WCD's box 5 - L1|WCD's box 5 - L1|Electrical socket box 5 - L1|
|19|CEE-form box 1 - L1|CEE-form box 1 - L1|CEE-form box 1 - L1|
|20|CEE-form box 1 - L2|CEE-form box 1 - L2|CEE-form box 1 - L2|
|21|CEE-form box 1 - L3|CEE-form box 1 - L3|CEE-form box 1 - L3|
|22|CEE-form box 2 - L1|CEE-form box 2 - L1|CEE-form box 2 - L1|
|23|CEE-form box 2 - L2|CEE-form box 2 - L2|CEE-form box 2 - L2|
|24|CEE-form box 2 - L3|CEE-form box 2 - L3|CEE-form box 2 - L3|
|25|CEE-form box 3 - L1|CEE-form box 3 - L1|CEE-form box 3 - L1|
|26|CEE-form box 3 - L2|CEE-form box 3 - L2|CEE-form box 3 - L2|
|27|CEE-form box 3 - L3|CEE-form box 3 - L3|CEE-form box 3 - L3|
|28|CEE-form box 4 - L1|CEE-form box 4 - L1|CEE-form box 4 - L1|
|29|CEE-form box 4 - L2|CEE-form box 4 - L2|CEE-form box 4 - L2|
|30|CEE-form box 4 - L3|CEE-form box 4 - L3|CEE-form box 4 - L3|
|31|CEE-form box 5 - L1|CEE-form box 5 - L1|CEE-form box 5 - L1|
|32|CEE-form box 5 - L2|CEE-form box 5 - L2|CEE-form box 5 - L2|
|33|CEE-form box 5 - L3|CEE-form box 5 - L3|CEE-form box 5 - L13|
|34|Afzuiging box 5 - L1|Afzuiging box 5 - L1|Extraction box 5 - L1|
|35|Afzuiging box 5 - L2|Afzuiging box 5 - L2|Extraction box 5 - L2|
|36|Afzuiging box 5 - L3|Afzuiging box 5 - L3|Extraction box 5 - L3|
|37|RESERVE - L2|Reserve - L2|Reserve - L3|
|38|Warmtepomp O-Nexus - L1|Warmtepomp O-Nexus - L1| Heatpump O-Nexus - L1|
|39|Regelkast warmtenet - L1|Regelkast warmtenet - L1|Controlbox heatnet - L1|
|40|SKID pomp warmtenet - L1|SKID pomp warmtenet - L1|SKID pump heatnet - L1|
|41|PV #1 - L1|PV #1 - L1|PV #1 - L1|
|42|PV #2 - L1|PV #2 - L1|PV #2 - L1|
|43|PV #3 - L1|PV #3 - L1|PV #3 - L1|
|44|PV #4 - L1|PV #4 - L1|PV #4 - L1|
# Legacy
**Can be found at https://gitlab.com/the-green-village/projects/energy/abb-scu100-testing**

This part of the documentation is for previous versions of this producer, the legacy code subheading describes files that are not currently used in this project.
## Legacy read-me:

This is a sped up version from the normal modbus producer which makes use of the fact that reading out 1 register is
equal in speed than reading out 125(max) neighbouring registers. This is done by dividing the entire list of register
mappings into different variables since the register numbers within these variables are relatively close together and
then finding the minimum amount of calls to make for each variable group after which returns a list. Since multiple
registers can belong to one value this list is then transform to matrix where the rows are value's and decoded
depended on value type(16-bit, 32-bit, signed, string, low-high resolution pair) and produced to kafka the register
mapping used is transformed from xlsx and removed some misplaced whitespaces into json file by
make_register_mapping.py

#### Energy consumption values with 100Wh or 100varh resolution
In mains there are a couple of energy values with '100' in the name. these have a resolution of 100 and are coupled to 
values with a lower resolution. For example:
'ACTIVE ENERGY L1' and 'ACTIVE ENERGY L1 100wh'. where the low resolution number is used to store the precise value but since this number is limited by
the 32 or 16 bit max integer value it eventually resets to zero. This is where the 100 value kicks in the provide low resolution data about big the value is which is then combined with the 
big resolution value to make up a value that is precise and can be larger than the max 32 bit integer.

