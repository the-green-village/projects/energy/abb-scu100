#!/bin/bash
source ~/producers/.kafka_envs

cd ~/producers/energy/abb-scu100
. env/bin/activate
python modbus3_producer.py
